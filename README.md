# Coq Confidentiality Proof 

Confidentiality.v contains the Coq source code of the proof. 
ConfidentialityProof.pdf is a document given the detailled steps of the proof.


# Publications

| Version | Reference |
| ------ | ------ |
| V1.0.0 | Formalizing the Relationship between Security Policies and Objectives in Software Architectures  European Dependable Computing Conference 2022, **<Under Submisison>**|
