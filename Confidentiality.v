Require Import List.
Include ListNotations.

Require Import Fin.

Parameter Component: Type.
Parameter Message: Type.
Parameter Payload : Type.


Parameter Pld: Message -> Payload.
Parameter Src: Message -> Component.
Parameter Rcv: Message -> Component.


Inductive Action: Type :=
    inject: Message -> Action
  | intercept: Message -> Action
  | get_rcv: Message -> Component -> Action
  | get_src: Message -> Component -> Action
  | get_pld: Message -> Payload -> Action
  | set_rcv: Message -> Component -> Action
  | set_src: Message -> Component -> Action
  | set_pld: Message -> Payload -> Action.


Inductive CompoundAction : Type :=
    BasicAction (c:Component) (a : Action)
  | Any
  | UnionAction (a1 a2 : CompoundAction)
  | SequenceAction (a1 a2 : CompoundAction)
  | RepeatAction (a: CompoundAction)
  | SkipAction.

Definition Behavior := nat -> (Component * Action).

Notation "c 'does' a" := (BasicAction c a) (at level 23,  no associativity).

(* atomic properties *)
Inductive Atom :=
| has_src (m: Message) (c: Component)
| has_rcv (m: Message) (c: Component)
| has_pld (m: Message) (d: Payload)
| E (ag: Component) (a: Action).

Inductive Formula :=
| AtomicFormula (a:Atom)
| IsFalse
| Implies (f1: Formula) (f2: Formula)
| Until (f1: Formula) (A: CompoundAction) (f2: Formula).

Coercion AtomicFormula: Atom >-> Formula.

Definition Not p := Implies p IsFalse.
Definition IsTrue := Not IsFalse.
Definition Or p q := Implies (Not p) q.
Definition And p q := Not (Or (Not p) (Not q)).
Definition Eventually A f := Until IsTrue A f.
Definition Globally A f := Not (Eventually A (Not f)).
Definition Leadsto f1 A f2 := Globally Any (Implies f1 (Eventually A f2)).
Definition AllA (ca: CompoundAction) (a: Formula) := Leadsto IsTrue ca a.
Definition ExA (ca: CompoundAction) (a: Formula) := Not (AllA ca (Not a)).
Definition H ag a := ExA (ag does a) IsTrue.
Definition Precede p A q := Not (Until (Not p) A q).

Notation "[[ A ]] p" := (AllA A p) (at level 10, no associativity).
Notation "<< A >> p" := (ExA A p) (at level 10, no associativity).
Notation "p ==> q" := (Implies p q) (at level 14, right associativity).
Notation "p --> q" := (Leadsto p Any q) (at level 49, right associativity).
Notation "p -[ A ]-> q" := (Leadsto p A q) (at level 49, right associativity).
Notation "p << q" := (Precede p Any q) (at level 15, no associativity).
Notation "p <[ A ]< q" := (Precede p A q) (at level 15, no associativity).
Notation "p || q" := (Or p q).
Notation "p && q" := (And p q).
Notation "! p" := (Not p) (at level 9, right associativity).

Parameter asat: (Component * Action) -> Atom -> Prop.
Parameter compat: Behavior -> nat -> CompoundAction -> Prop.

Fixpoint sat B f : Prop :=
  match f with
   AtomicFormula a => asat (B 0) a
 | IsFalse => False
 | Implies f1 f2 => (sat B f1) -> (sat B f2)
 | Until f1 A f2 => exists t,
    sat (fun i => B (t+i)) f2 /\
    (forall t', t' < t -> sat (fun i => B (t'+i)) f1)
  end.

Notation "B |- f" := (sat B f) (at level 50, no associativity).

Axiom HasSrc : forall B A r m, (B |- Eventually A (has_src m r)) -> r = Src m.
Axiom HasRcv : forall B A r m, (B |- Eventually A (has_rcv m r)) -> r = Rcv m.
Axiom HasPld : forall B A r m, (B |- Eventually A (has_pld m r)) -> r = Pld m.
Lemma satNot_elim: forall B p, (B |- !p) -> not (B |- p).
Proof.
  intros; assumption.
Qed.
Lemma satImpl_elim: forall B p q, (B |- p ==> q) -> ((B |- p) -> (B |- q)).
Proof.
  intros.
  apply H0.
  apply H1.
Qed.
Lemma satImpl_intro: forall B p q, ((B |- p) -> (B |- q)) -> (B |- p ==> q).
Proof.
  intros.
  simpl; auto.
Qed.
Require Import Classical.
Lemma satAnd_elim: forall B p q, (B |- p && q) -> ((B |- p) /\ (B |- q)).
Proof.
  intros.
  simpl in H0.
  apply NNPP; intro.
  auto.
Qed.
Lemma satEventuallyAnd_elim: forall B p q A, (B |- Eventually A (p && q)) -> ((B |- Eventually A p) /\ (B |- Eventually A q)).
Proof.
  intros.
  destruct H0 as [t H0].
  destruct H0.
  apply NNPP; intro.
  simpl in H2.
  induction H2.
  split; exists t; split; intros; auto; clear H1.
  apply satAnd_elim in H0; apply H0.
  apply satAnd_elim in H0; apply H0.
Qed.
Lemma eventually_imply: forall p q A, (forall B, B |- p ==> q) -> forall B, B |- Eventually A p ==> Eventually A q.
Proof.
  intros.
  apply satImpl_intro; intro.
  simpl in H1.
  destruct H1 as [t H1].
  destruct H1.
  specialize (H0 (fun i : nat => B (t + i))).
  exists t; split; auto.
Qed.
Lemma gImp_eventually: forall B p q A, (B |- Globally A (p ==> q)) -> (B |- Eventually A p) -> (B |- Eventually A q).
Proof.
  intros.
  destruct H1 as [t H1]; destruct H1.
  exists t; split; auto.
  simpl in H0.
  apply NNPP; intro.
  apply H0; clear H0.
  exists t; split; intros; auto.
Qed.
Lemma prec_eventually: forall B p q A, (B |- p <[ A ]< q) -> (B |- Eventually A  q) -> (B |- Eventually A p).
Proof.
  intros.
  simpl in H0;
  simpl in  H1.
  simpl.
  destruct H1 as [t H1]; destruct H1.
  apply NNPP; intro.
  destruct H0.
  exists t ; split.
  apply H1.
  intros.
  destruct H3.
  exists t'; split.
  apply H4.
  auto. 
Qed.


Axiom axiomHImpE : forall B c1 p, B |- (H c1 p) ==> (E c1 p).
Axiom axiomInjectPrecIntercept : forall B c1 c2 m, B |- H c2 (inject m) << E c1 (intercept m).
Axiom axiomInterceptHasPldImpEGetPld : forall B c m d, B |- (H c (intercept m) && (has_pld m d)) << E c (get_pld m d).



Definition PayloadConfidenciality B c1 c2 :=
  forall c3 m d, not (In c3 [c1; c2]) -> (B |- Eventually Any (E c3 (get_pld m d))) ->
    not (B |- (H c1 (inject m) && has_rcv m c2) << (E c3 (get_pld m d))).

Record AllowedGetPld : Type :=  {
  msg: Message ;
  comp : Component;
}.

Definition restrictiveGetPld (p:AllowedGetPld) m B :=
  (forall c, (B |- Eventually Any (E c (inject m)) -> (msg p = m /\ Rcv m = comp p)))
    /\
  (forall c d, (B |- Eventually Any (E c (get_pld m d))) -> (msg p = m /\ c = comp p)). (* When get_pld is done respect the policy *)


Definition connectorRestritiveGetPld p B :=
  forall c m, (B |- Eventually Any (E c (intercept m))) -> restrictiveGetPld p m B.


Theorem confidentialityHold:
  forall B c1 c2 p, connectorRestritiveGetPld p B -> PayloadConfidenciality B c1 c2.
Proof.
  (* step 1 *)
  intros B c1 c2 p connector_restritive_get_pld.
  unfold PayloadConfidenciality, connectorRestritiveGetPld, restrictiveGetPld  in *.
  intros c3 m d c3_not_in_c1_c2 c3_eventually_enable_to_get_d c1_injected_m_to_c2.
  specialize (connector_restritive_get_pld c3 m).
  
  (* step 2 *)
  generalize (axiomInterceptHasPldImpEGetPld B c3 m d). intro axiomInterceptHasPldImpGetPld_c3_m_d.
  generalize (prec_eventually _ _ _ _ axiomInterceptHasPldImpGetPld_c3_m_d c3_eventually_enable_to_get_d). clear axiomInterceptHasPldImpGetPld_c3_m_d;
  intro c3_eventually_intercept_m_and_hasPld_m_d.
  apply satEventuallyAnd_elim in c3_eventually_intercept_m_and_hasPld_m_d.
  destruct c3_eventually_intercept_m_and_hasPld_m_d as [c3_eventually_intercept_m _].
  
  (* step 3 *)
  generalize(fun B => axiomHImpE B c3 (intercept m)); intro axiomHImpE_c3_intercept_m.
  generalize (eventually_imply _ _ Any axiomHImpE_c3_intercept_m B); clear axiomHImpE_c3_intercept_m;
  intro ev_c3_intercept_m_impl_enable_c3_intercept_m.
  generalize (satImpl_elim B _ _ ev_c3_intercept_m_impl_enable_c3_intercept_m c3_eventually_intercept_m);
  clear ev_c3_intercept_m_impl_enable_c3_intercept_m ev_c3_intercept_m_impl_enable_c3_intercept_m; 
  intro c3_eventually_e_intercept_m.  
  
  (* step 4 *)
  apply connector_restritive_get_pld in c3_eventually_e_intercept_m as restritive_get_pld; clear connector_restritive_get_pld c3_eventually_e_intercept_m.
  
  
  (* step 5 *)
  destruct restritive_get_pld as [eventually_e_inj_m_impl_valid_policy eventually_e_get_m_d_impl_valid_policy]. 
  specialize (eventually_e_inj_m_impl_valid_policy c1).
  specialize (eventually_e_get_m_d_impl_valid_policy c3 d).
  destruct (eventually_e_get_m_d_impl_valid_policy c3_eventually_enable_to_get_d) ; clear eventually_e_get_m_d_impl_valid_policy.
  subst.
  
  (* step 6 *)
  generalize(axiomInterceptHasPldImpEGetPld B (comp p) (msg p) d); intro axiomInterceptHasPldImpEGetPld_comp_p_msg_p_d.
  generalize (prec_eventually _ _ _ _ axiomInterceptHasPldImpEGetPld_comp_p_msg_p_d c3_eventually_enable_to_get_d). 
  clear axiomInterceptHasPldImpEGetPld_comp_p_msg_p_d.
  intro eventually_h_comp_p_intercept_msg_p.
  apply satEventuallyAnd_elim in eventually_h_comp_p_intercept_msg_p.
  destruct eventually_h_comp_p_intercept_msg_p as [eventually_h_comp_p_intercept_msg_p _].
  
  
  (* step 7 *)
  generalize (axiomInjectPrecIntercept B (comp p) c1 (msg p)); intro axiomInjectPrecIntercept_comp_p_c1_msg_p_d.
  generalize(fun B => axiomHImpE B (comp p) (intercept (msg p))); intro axiomHImpE_comp_p_intercept_msg_p.
  generalize (eventually_imply _ _ Any axiomHImpE_comp_p_intercept_msg_p B); 
  clear axiomHImpE_comp_p_intercept_msg_p; 
  intro ev_H_compP_intercept_msgP.
  generalize (satImpl_elim B _ _ ev_H_compP_intercept_msgP eventually_h_comp_p_intercept_msg_p); 
  clear ev_H_compP_intercept_msgP;
  intro H_c1_inject_msgP_impl_E_c1_inject_msgP.
  generalize (prec_eventually _ _ _ _ axiomInjectPrecIntercept_comp_p_c1_msg_p_d H_c1_inject_msgP_impl_E_c1_inject_msgP).
  clear axiomInjectPrecIntercept_comp_p_c1_msg_p_d H_c1_inject_msgP_impl_E_c1_inject_msgP; 
  intro ev_c1_inject_msgP.
  
  
  (* step 8 *)
  generalize(fun B => axiomHImpE B c1 (inject (msg p))); intro axiomHImpE_c1_inject_msgP.
  generalize (eventually_imply _ _ Any axiomHImpE_c1_inject_msgP B); 
  clear axiomHImpE_c1_inject_msgP;
  intro H_c1_inject_msgP_impl_E_c1_inject_msgP.
  generalize (satImpl_elim B _ _ H_c1_inject_msgP_impl_E_c1_inject_msgP ev_c1_inject_msgP);
  clear H_c1_inject_msgP_impl_E_c1_inject_msgP;
  intro E_c1_inject_msgP.
  
  (* step 9 *)
  generalize (eventually_e_inj_m_impl_valid_policy E_c1_inject_msgP).
  clear E_c1_inject_msgP;
  intro policy.
  destruct policy as [_ policyRight].
  rewrite <- policyRight in *; clear policyRight.
  
  (* step 10 *)
  generalize (prec_eventually _ _ _ _ c1_injected_m_to_c2 c3_eventually_enable_to_get_d). 
  clear c1_injected_m_to_c2 c3_eventually_enable_to_get_d; 
  intro ev_h_c1_inject_msgP_and_hasRcv_msgP_c2.
  apply satEventuallyAnd_elim in ev_h_c1_inject_msgP_and_hasRcv_msgP_c2.
  destruct ev_h_c1_inject_msgP_and_hasRcv_msgP_c2 as [_ ev_hasRcv_msg2_c2].
  apply HasRcv in ev_hasRcv_msg2_c2.
  subst.
  apply c3_not_in_c1_c2.
  simpl.
  auto.
Qed.

